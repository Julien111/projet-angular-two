import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'pokemonTypeColor',
})
export class PokemonTypeColorPipe implements PipeTransform {
  transform(type: string): string {
    let color: string;
    switch (type) {
      case 'Feu':
        color = 'red';
        break;
      case 'Eau':
        color = 'blue';
        break;
      case 'Insecte':
        color = 'brown';
        break;
      case 'Plante':
        color = 'green';
        break;
      case 'Normal':
        color = 'dimgrey';
        break;
      case 'Vol':
        color = 'lightblue';
        break;
      case 'Poison':
        color = 'navy';
        break;
      case 'Fée':
        color = 'pink';
        break;
      case 'Psy':
        color = 'purple';
        break;
      case 'Electrik':
        color = 'yellow';
        break;
      case 'Combat':
        color = 'orange';
        break;
      default:
        color = 'grey';
        break;
    }

    return 'bg-' + color;
  }
}
