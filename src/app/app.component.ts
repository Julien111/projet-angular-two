import { Component, OnInit } from '@angular/core';
import { POKEMONS } from './mock-pokemon-list';
import { Pokemon } from './pokemon';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-root',
  templateUrl: './templates/app.component.html',
  styleUrls: ['./styles/app.component.css'],
})
export class AppComponent implements OnInit {
  pokemonList: Pokemon[] = POKEMONS;
  pokemonSelected: Pokemon|undefined;

  ngOnInit(): void {
    console.table(this.pokemonList);
  }

  selectPokemon(pokemonId: string): void {
    let index = +pokemonId;
    
    //code avec une boucle simple
    // for(let pokemon of this.pokemonList) {
    //     if(pokemon.id === index){
    //       console.log(`Le pokémon est : ${pokemon.name}`);
    //     }
    //     else{
    //       console.log("Le pokemon n'existe pas");
    //     }
    // }

    const pokemon: Pokemon | undefined = this.pokemonList.find(
      (pok) => pok.id == index
    );
    if (pokemon) {
      console.log(`Le pokémon est : ${pokemon.name}`);
      this.pokemonSelected = pokemon;
    } else {
      console.log("Le pokemon n'existe pas");
      this.pokemonSelected = pokemon;
    }
  }

  selectPokemonIndex(index: number): void {
    console.log(`Le pokémon est : ${this.pokemonList[index].name}`);
  }

  onSubmit(f: NgForm) {
    this.selectPokemonIndex(f.value.index);
  }
}
